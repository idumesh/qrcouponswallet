package com.umesh.qrcouponswallet.utils;

/**
 * Created by harshita on 12/05/16.
 */
public class Constants {
    public static final int SUCCESS = 1;
    public static final int RC_QR_CODE_SCAN = 21;

    public static final int RC_LOCATION = 51;
    public static final boolean TEST = false;


}
