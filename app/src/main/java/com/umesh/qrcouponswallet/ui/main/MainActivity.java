package com.umesh.qrcouponswallet.ui.main;

/**
 * Created by harshita on 11/05/16.
 */

import android.Manifest;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.samples.vision.googlevision.QRcodeScanner;
import com.google.android.gms.vision.barcode.Barcode;
import com.umesh.qrcouponswallet.R;
import com.umesh.qrcouponswallet.coupons.Coupon;
import com.umesh.qrcouponswallet.coupons.CouponManager;
import com.umesh.qrcouponswallet.coupons.CouponRecyclerAdapter;
import com.umesh.qrcouponswallet.dbhelper.DatabaseHelper;
import com.umesh.qrcouponswallet.parser.QRcodeParser;
import com.umesh.qrcouponswallet.utils.Constants;
import com.umesh.qrcouponswallet.utils.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private static final String PROXMITY_ALERT_INTENT = "com.umesh.qrcodewallet.ProximityAlert";
    private static final long POINT_RADIUS = 100; // in meters
    CouponRecyclerAdapter mAdapter;
    DatabaseHelper dbHelper = null;
    private FloatingActionButton fab;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primary);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, QRcodeScanner.class);
                startActivityForResult(intent, Constants.RC_QR_CODE_SCAN);
            }
        });

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        dbHelper = new DatabaseHelper(getApplicationContext());

        populateTestDataOnce();

        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        setDataToAdapter();
        recyclerView.setAdapter(mAdapter);
        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(itemDecoration);


        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            //Update Shared Preferences
        } else {
            requestLocationPermission();
        }

        onNewIntent(getIntent());

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_scan_qr) {
            Intent intent = new Intent(MainActivity.this, QRcodeScanner.class);
            startActivityForResult(intent, Constants.RC_QR_CODE_SCAN);
            return true;
        } else if (id == R.id.action_location) {
            LocationManager lm = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            boolean gps_enabled = false;
            boolean network_enabled = false;

            try {
                gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            try {
                network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            if (!gps_enabled && !network_enabled) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                alertDialog.setTitle("Enable Location");
                alertDialog.setMessage("Please enable location in settings to get proximity alerts.");
                alertDialog.setIcon(R.drawable.location_pin);

                alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                alertDialog.show();
                return true;
            } else {
                Toast.makeText(MainActivity.this, "Your location is already enabled in settings.", Toast.LENGTH_LONG).show();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            int couponId = bundle.getInt("id");
            if (Constants.TEST)
                Toast.makeText(MainActivity.this, "onNewIntent-Nearbuy Coupon: " + couponId, Toast.LENGTH_SHORT).show();
            mAdapter.nearbuyCoupon(couponId);
        } else {
            if (Constants.TEST)
                Toast.makeText(MainActivity.this, "onNewIntent- -1 ", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.RC_QR_CODE_SCAN) {
            if (resultCode == Constants.SUCCESS) {
                Barcode barcode = data.getParcelableExtra("QRcode");
                String qrCodeText = barcode.displayValue;

                Log.d(TAG, "QRCode read: " + qrCodeText);
                Toast.makeText(MainActivity.this, "QR code:" + "\n" +
                        qrCodeText, Toast.LENGTH_LONG).show();

                Coupon coupon = QRcodeParser.getInstance().parse(MainActivity.this, qrCodeText);
                if (coupon != null) {
                    CouponManager.getInstance().addToList(coupon);

                    if (mAdapter != null) {
                        mAdapter.addItem(coupon, mAdapter.getItemCount() - 1);
                        dbHelper.addData(coupon);
                        addProximityAlert(coupon);
                    }
                }

            } else {
                Log.d(TAG, "No QR Code captured, intent data is null");
            }
        } else {

            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void setDataToAdapter() {
        List list = dbHelper.getData();
        mAdapter = new CouponRecyclerAdapter(list);
    }

    void populateTestDataOnce() {
        SharedPreferences myPrefs = getSharedPreferences("init", MODE_PRIVATE);
        boolean intilisationDone = myPrefs.getBoolean("done", false);
        if (!intilisationDone) {
            ArrayList<Coupon> list = CouponManager.getInstance().getDataSet(MainActivity.this);
            for (Coupon c : list) {
                dbHelper.addData(c);
            }

            SharedPreferences.Editor prefsEditor = myPrefs.edit();
            prefsEditor.putBoolean("done", true);
            prefsEditor.commit();
        }
    }

    /**
     * Handles the requesting of the location permission.  This includes
     * showing a "Snackbar" message of why the permission is needed then
     * sending the request.
     */
    private void requestLocationPermission() {
        Log.w(TAG, "Location permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};

        ActivityCompat.requestPermissions(this, permissions, Constants.RC_LOCATION);
        return;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != Constants.RC_LOCATION) {
            Log.d(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Location permission granted - initialize the camera source");

            return;
        }

        Log.e(TAG, "Permission not granted: results len = " + grantResults.length +
                " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("WalletQR")
                .setMessage(R.string.no_location_permission)
                .setPositiveButton(R.string.ok, listener)
                .show();
    }


    private void addProximityAlert(Coupon coupon) {
        double latitude = coupon.getLatitude();
        double longitude = coupon.getLongitude();
        Intent intent = new Intent(PROXMITY_ALERT_INTENT);
        Bundle bundle = new Bundle();
        bundle.putString("shop", coupon.getShop());
        bundle.putString("discount", coupon.getDiscount());
        bundle.putString("expiry", coupon.getExpiryString());
        int id = coupon.getID();
        bundle.putInt("id", id);

        intent.putExtras(bundle);
        if (Constants.TEST)
            Toast.makeText(MainActivity.this, "addProximityAlert-Nearbuy Coupon: " + coupon.getID(), Toast.LENGTH_SHORT).show();

        PendingIntent proximityIntent = PendingIntent.getBroadcast(this, id, intent, 0);
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            locationManager.addProximityAlert(
                    latitude, // the latitude of the central point of the alert region
                    longitude, // the longitude of the central point of the alert region
                    POINT_RADIUS, // the radius of the central point of the alert region, in meters
                    600000, // time for this proximity alert, in milliseconds, or -1 to indicate no                           expiration
                    proximityIntent // will be used to generate an Intent to fire when entry to or exit from the alert region is detected
            );

            Toast.makeText(getApplicationContext(), "Location Alert Added: \n" + latitude + ", " + longitude, Toast.LENGTH_SHORT).show();
        } else {
            requestLocationPermission();
        }


    }
}