package com.umesh.qrcouponswallet.coupons;

import com.j256.ormlite.field.DatabaseField;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by harshita on 11/05/16.
 */
public class Coupon {

    @DatabaseField
    String mDiscount;
    @DatabaseField
    String mLocation;
    @DatabaseField
    String mShop;
    @DatabaseField
    String mExpiryDate;
    @DatabaseField
    double mLatitude;
    @DatabaseField
    double mLongitude;
    @DatabaseField
    private int id;
    @DatabaseField
    private boolean nearbuy;

    public Coupon(String discount, String shop, String expiry, String location, int id) {
        mDiscount = discount;
        mShop = shop;
        mExpiryDate = expiry;
        mLocation = location;
        this.id = id;
        this.nearbuy = false;
    }

    public Coupon() {
    }

    public String getDiscount() {
        return mDiscount;
    }

    public String getLocation() {
        return mLocation;
    }

    public String getShop() {
        return mShop;
    }

    public Date getExpiry() {

        Date date = null;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = formatter.parse(mExpiryDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public String getExpiryString() {
        return mExpiryDate;
    }

    public void setLocation(double latitude, double longitude) {

        mLatitude = latitude;
        mLongitude = longitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void updateID(int id) {
        this.id = id;
    }

    public int getID() {
        return id;
    }

    public void updateNearbuy(boolean status) {
        nearbuy = status;
    }

    public boolean isNearbuy() {
        return nearbuy;
    }
}
