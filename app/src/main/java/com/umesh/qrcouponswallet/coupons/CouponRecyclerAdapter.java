package com.umesh.qrcouponswallet.coupons;


import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.umesh.qrcouponswallet.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CouponRecyclerAdapter extends RecyclerView
        .Adapter<CouponRecyclerAdapter.CouponHolder> {
    private static String LOG_TAG = "CouponRecyclerAdapter";
    private static MyClickListener myClickListener;
    private List<Coupon> mDataset;

    public CouponRecyclerAdapter(List<Coupon> myDataset) {
        mDataset = myDataset;
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    @Override
    public CouponHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item, parent, false);

        CouponHolder dataObjectHolder = new CouponHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(CouponHolder holder, int position) {
        holder.discount.setText(mDataset.get(position).getDiscount());
        holder.shopName.setText(mDataset.get(position).getShop());
        holder.expiryDate.setText(mDataset.get(position).getExpiryString());


        Date expiryDate = mDataset.get(position).getExpiry();
        Date date = null;
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
        try {
            date = dateFormatter.parse(dateFormatter.format(new Date()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int result = date.compareTo(expiryDate);
        if (result == 0) {
            Log.d("Timing", " Are equal");

            holder.v.setBackgroundColor(Color.GREEN);
            holder.expiryDate.setText("today!!!");
        } else {
            holder.v.setBackgroundColor(Color.WHITE);

        }

        if (mDataset.get(position).isNearbuy()) {
            holder.nearbuy.setVisibility(View.VISIBLE);
        } else {
            holder.nearbuy.setVisibility(View.GONE);
        }


        Log.d("Timing", " " + expiryDate.toString() + "  " + date.toString());

    }

    public void addItem(Coupon dataObj, int index) {
        mDataset.add(dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void nearbuyCoupon(int id) {
        for (Coupon c : mDataset) {
            if (c.getID() == id) {
                c.updateNearbuy(true);
                notifyDataSetChanged();
            }
        }
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }

    public static class CouponHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {

        TextView discount;
        TextView shopName;
        TextView expiryDate;
        TextView nearbuy;
        View v;

        public CouponHolder(View itemView) {
            super(itemView);
            discount = (TextView) itemView.findViewById(R.id.textView1);
            shopName = (TextView) itemView.findViewById(R.id.textView2);
            expiryDate = (TextView) itemView.findViewById(R.id.textView4);
            nearbuy = (TextView) itemView.findViewById(R.id.textView5);

            v = itemView;
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (myClickListener != null) myClickListener.onItemClick(getPosition(), v);
        }
    }

}
