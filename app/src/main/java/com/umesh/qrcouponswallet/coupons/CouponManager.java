package com.umesh.qrcouponswallet.coupons;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

/**
 * Created by harshita on 11/05/16.
 */
public class CouponManager {
    private static CouponManager ourInstance = new CouponManager();
    private ArrayList<Coupon> list = new ArrayList<>();

    private CouponManager() {
    }

    public static CouponManager getInstance() {
        return ourInstance;
    }

    public void addToList(Coupon coupon) {
        list.add(coupon);
    }

    public ArrayList<Coupon> getDataSet(Context ctx) {

        addToList(new Coupon("10%", "Domino's", "13/05/2016", "28.4669-77.0665", getId(ctx)));
        addToList(new Coupon("15%", "Pizza Hut", "15/05/2016", "28.4669-77.0665", getId(ctx)));
        addToList(new Coupon("5%", "Venky's", "16/05/2016", "28.4669-77.0665", getId(ctx)));
        addToList(new Coupon("12%", "Schindler", "18/05/2016", "28.4669-77.0665", getId(ctx)));
        addToList(new Coupon("15%", "OM Sweets", "19/05/2016", "28.4669-77.0665", getId(ctx)));
        addToList(new Coupon("10%", "Domino's", "15/05/2016", "28.4669-77.0665", getId(ctx)));
        addToList(new Coupon("15%", "Burger king", "13/05/2016", "28.4669-77.0665", getId(ctx)));
        addToList(new Coupon("5%", "MacDonalds", "16/05/2016", "28.4669-77.0665", getId(ctx)));
        addToList(new Coupon("10%", "Domino's", "17/05/2016", "28.4669-77.0665", getId(ctx)));
        addToList(new Coupon("12%", "MacDonalds", "18/05/2016", "28.4669-77.0665", getId(ctx)));

        ArrayList<Coupon> destination = new ArrayList<>(list);

        return destination;
    }


    public int getId(Context context) {
        SharedPreferences myPrefs = context.getSharedPreferences("init", Context.MODE_PRIVATE);
        int id = myPrefs.getInt("lastID", 108);

        id++;

        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putInt("lastID", id);
        prefsEditor.commit();

        return id;
    }

}
