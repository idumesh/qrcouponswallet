package com.umesh.qrcouponswallet.dbhelper;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.umesh.qrcouponswallet.coupons.Coupon;

import java.sql.SQLException;
import java.util.List;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "ormlitecoupons.db";
    private static final int DATABASE_VERSION = 1;
    private Context context;
    private Dao<Coupon, Integer> couponDao = null;
    private RuntimeExceptionDao<Coupon, Integer> couponRuntimeDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, Coupon.class);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);

        }

    }


    public RuntimeExceptionDao<Coupon, Integer> getCouponDao()

    {
        if (couponRuntimeDao == null) {
            couponRuntimeDao = getRuntimeExceptionDao(Coupon.class);
        }
        return couponRuntimeDao;
    }

    //method for list of Coupon
    public List<Coupon> getData() {
        DatabaseHelper helper = new DatabaseHelper(context);
        RuntimeExceptionDao<Coupon, Integer> couponDao = helper.getCouponDao();
        List<Coupon> list = couponDao.queryForAll();
        return list;
    }

    //method for insert data
    public int addData(Coupon Coupon) {
        RuntimeExceptionDao<Coupon, Integer> dao = getCouponDao();
        int i = dao.create(Coupon);
        return i;
    }

    //method for delete all rows
    public void deleteAll() {
        RuntimeExceptionDao<Coupon, Integer> dao = getCouponDao();
        List<Coupon> list = dao.queryForAll();
        dao.delete(list);
    }

    @Override
    public void close() {
        super.close();
        couponRuntimeDao = null;
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource, Coupon.class, true);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }
}