package com.umesh.qrcouponswallet.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.umesh.qrcouponswallet.R;
import com.umesh.qrcouponswallet.ui.main.MainActivity;
import com.umesh.qrcouponswallet.utils.Constants;

public class ProximityIntentReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String key = LocationManager.KEY_PROXIMITY_ENTERING;

        Bundle bundle = intent.getExtras();
        int id = bundle.getInt("id");
        String shop = bundle.getString("shop");
        String discount = bundle.getString("discount");

        Boolean entering = intent.getBooleanExtra(key, false);
        if (entering) {
            Log.d(getClass().getSimpleName(), "entering");
        } else {
            Log.d(getClass().getSimpleName(), "exiting");
        }
        Intent resultIntent = new Intent(context, MainActivity.class);
        Bundle bundle1 = new Bundle();
        bundle1.putInt("id", id);

        resultIntent.putExtras(bundle1);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);

        if (Constants.TEST)
            Toast.makeText(context, "onReceive-Nearbuy Coupon: " + id, Toast.LENGTH_SHORT).show();

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.percentage);

        Notification notification = new Notification();
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;


        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.icon)
                        .setLargeIcon(largeIcon)
                        .setContentTitle("Proximity Alert")
                        .setContentText("Redeem this coupon: \n" + discount + " at " + shop)
                        .setAutoCancel(true);
        mBuilder.setContentIntent(resultPendingIntent);

        mBuilder.setDefaults(notification.defaults);

        int mNotificationId = id;
        NotificationManager mNotifyMgr =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

}
