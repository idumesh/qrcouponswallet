package com.umesh.qrcouponswallet.parser;

import android.content.Context;

import com.umesh.qrcouponswallet.coupons.Coupon;
import com.umesh.qrcouponswallet.coupons.CouponManager;

/**
 * Created by harshita on 11/05/16.
 */
public class QRcodeParser {
    private static QRcodeParser ourInstance = new QRcodeParser();

    private QRcodeParser() {
    }

    public static QRcodeParser getInstance() {
        return ourInstance;
    }

    public Coupon parse(Context context, String qrCodeText) {
        String delims = ";";
        String[] tokens = qrCodeText.split(delims);
        String discount = null, expiry = null, location = null, shop = null;
        int count = 0;
        for (int i = 0; i < tokens.length; i++) {
            String delims2 = ":";
            String[] labels = tokens[i].split(delims2);
            String escaped = labels[0].replace(" ", "");

            if ("discount".equalsIgnoreCase(escaped)) {
                discount = labels[1];
            } else if ("expiry".equalsIgnoreCase(escaped)) {
                expiry = labels[1];
            } else if ("location".equalsIgnoreCase(escaped)) {
                location = labels[1];

            } else if ("shop".equalsIgnoreCase(escaped)) {
                shop = labels[1];

            }

        }
        Coupon coupon = null;

        if (discount != null && expiry != null && location != null && shop != null) {
            coupon = new Coupon(discount, shop, expiry, location, CouponManager.getInstance().getId(context));

            double latitude, longitude;
            String delimslatlong = "-";
            String[] latlong = location.split(delimslatlong);
            latitude = Double.parseDouble(latlong[0]);
            longitude = Double.parseDouble(latlong[1]);

            coupon.setLocation(latitude, longitude);
        }
        return coupon;
    }


}
